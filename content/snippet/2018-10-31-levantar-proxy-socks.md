---
title: Crear proxy Sock5
type: snippet
Date: 2018-10-31
tags: [snippets]
---

```
/usr/bin/ssh -oStrictHostKeyChecking=no -i /home/${USER}/.ssh/id_rsa -ND 0.0.0.0:10080 ${USER}@localhost
```

---

### Referencias

Que es SOCKS -> [Wikipedia](https://es.wikipedia.org/wiki/SOCKS)
