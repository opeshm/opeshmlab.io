---
title: Redirecciones https con .htaccess
type: snippet
Date: 2019-01-23
tags: [snippets, apache]
---

#### Descripción

Crear una redirección de http a https dentro del archivo .htaccess. En este caso, crear las redireccion de http://opeshm.gitlab.io a https://opeshm.gitlab.io

<!--more-->

#### Configuraciones

Redirigir todas las peticiones http a https:
```
RewriteEngine On
RewriteCond %{SERVER_PORT} 80
RewriteRule ^(.*)$ https://opeshm.gitlab.io/$1 [R,L]
```

Si queremos redirigir solo las peticiones de un host concreto:
```
RewriteEngine On
RewriteCond %{HTTP_HOST} ^opeshm\.gitlab\.io [NC]
RewriteCond %{SERVER_PORT} 80
RewriteRule ^(.*)$ https://opeshm.gitlab.io/$1 [R,L]
```

> R=301 Hace referencia al tipo de redireccion:
> 301: Redirección permanente.
> 302: Redirección temporal.
