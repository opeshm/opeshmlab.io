---
title: Vitaminas para Bash
type: snippet
Date: 2018-11-16
tags: [snippets, bash]
---

#### Descripción

Añadir funcionalidad extra a bash.

<!--more-->

##### bash-utils

```sh
git clone https://github.com/opeshm/bash-utils.git ~/.bash-utils
~/.bash-utils/install.sh
```

##### bash-it
```sh
git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash_it
~/.bash_it/install.sh
```

##### Referencias

- [Bash Utils](https://github.com/opeshm/bash-utils): https://github.com/opeshm/bash-utils
- [Bash-It](https://github.com/bash-it/bash-it): https://github.com/bash-it/bash-it
