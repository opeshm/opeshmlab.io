---
title: Certificados autofirmados
type: snippet
Date: 2018-11-16
tags: [snippets, openssl]
---

#### Descripión

Crear certificados SSL autofirmados 

<!--more-->

```
openssl req -newkey rsa:2048 -nodes -keyout certificate.key -out certificate.cer
openssl x509 -signkey certificate.key -in certificate.cer -req -days 365 -out certificate.crt
```
