---
title: Limpiar identidades de SSH
type: snippet
Date: 2018-12-19
tags: [snippets, openssl, ssh]
---

#### Descripción

El agente del ssh está cacheando las identidades y queresmos limpiarlas.

<!--more-->

#### Comando

```
ssh-add -D
```

##### Extracto del man

```
...
     -D      Deletes all identities from the agent.
...
```
