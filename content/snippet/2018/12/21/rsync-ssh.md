---
title: Usar rsync con ssh
type: snippet
Date: 2018-12-21
tags: [snippets, rsync, ssh]
---

#### Descripción

Ejecutar rsync y el origen o el destino es una máquina remota

<!--more-->

#### Comando

```bash
rsync -avz -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ~/.ssh/idrsa" --progress origen destino

```

##### Extracto del man rsync

```man
...
     -a, --archive               archive mode; equals -rlptgoD (no -H,-A,-X)
     -r, --recursive             recurse into directories
     -l, --links                 copy symlinks as symlinks
     -p, --perms                 preserve permissions
     -t, --times                 preserve modification times
     -g, --group                 preserve group
     -o, --owner                 preserve owner (super-user only)
     -D                          same as --devices --specials
     -v, --verbose               increase verbosity
     -z, --compress              compress file data during the transfer
...
```
