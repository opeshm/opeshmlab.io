---
title: Sobre mi
subtitle: ¿Conoce los veneficios del software libre?
comments: false
---

Quiero pensar que todo comenzó con un Commodore C64 y luego más tarde con un
Commodore Amiga 500. Por aquellos años yo no sabia que era la informática y
mucho menos que era la programación, pero ahí estab yo mirando revistas que me
prestaban, picando el codigo que venia dentro y modificando algunas chorradas y
viendo a ver que pasaba...

Realmente creo en el Software Libre, y es por eso por lo que dejo este
rinconcito perdido en este inmeso espacio (y a veces vacio) que es internet.

## El por qué hago esto

Como es habitual, estoy acostumbrado a buscar en internet, información,
documentación, tutoriales, manuales, cookbooks, videos, software,
configuraciones... por donde iba... ah! sí. No toques mi mueble bar! Bueno, pues
esto no es más que un recopilatorio de snippets, scripts, configuraciones, etc
que me han ido haciendo falta, como repositorio personal
