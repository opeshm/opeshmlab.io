---
title: "Mi blog"
subtitle: "Gitlab pages con Hugo"
date: 2018-10-31
draft: false
tags: ["howto", "gitlab"]
---

Lo dicho, el primer de lo que espero sea una larga lista de post. Y como he dicho en el post de bienvenida, que mejor manera de empezar que contando como he montado este mismo blog. Asi qué, alla vamos

<!--more-->

Lo primero, un poco de contexto. La idéa que tenía para el blog no era la de un sitio donde escribir mis mierdas, sino más la de un lugar donde poder tener guardado y clasificado distintas soluciones a problemas, snippets de código, documentación escrita, y alguna que otra cosilla más, pero el hecho de pensar en drupal, wordpress, joomla (¿se sigue usando este?), o cualquier otro CMS al uso, me daba una pereza horrorosa. Me parecen herramientas extremadamente pesadas para la función que desenpeñan. Pueden estar bien para personas que sepan nada en absoluto sobre "web", sin embargo, para cualquier otra persona que se haya metido un poco en el mundillo, existen herramientas un poco más ajustadas a las necesidades propias de crear un blog. Para mi este es el caso de los _generadores de sites estaticos_ [[¹]](#ref_1), que siempre me han parecido herramientas sencillas de usar, que no requiere demasiado esfuerzo y que cumplen muy bien con su función.

Tras buscar y analizar algunas herramientas [[²]](#ref_2)[[³]](#ref_3), me encontré con Hugo [[⁴]](#ref_4), un proyecto en Go, Software Libre (condición indispensable), con una comunidad bastante activa y con un gran catalogo de plugins y temas. Así que tras una primera toma de contacto, y habiendo probado alguna que otra alternativa (Pelican, Jekyll, Hexo, ...) me convenció.

Por otro lado, me tocaba pensar en el alojamiento web para esto. Teniendo en cuenta que me había quitado de un plumazo los CMS que necesitan de compiladores / interpretes del lado del servidor, y que lo único que necesitaba era algún servicio que sirviese (valga la _rebuznancia_) páginas estaticas, me puse a mirar y me encontré con las "pages" de github [[⁵]](#ref_5) y gitlab[[⁶]](#ref_6), ambas soluciones perfectas para lo que necesitaba, lo único que me quedaba era ver cual de los dos escogía... sin duda, gitlab. ¿Por qué? mira los servicios ofrecido por uno y por otro (teniendo en cuenta que no me quiero gastar un duro), mira la herramienta de CI/CD que tiene GitLab. Y otra cosa importantísima, tienen todo el código publicado [[⁷]](#ref_7)

Pues ya está, Hugo + GitLab Pages, ahí lo tenía, solo me quedaba ponerme manos a la obra.

# Pasos para crear el blog.

> Disclaimer: Todos estos pasos no me los he inventado yo (no soy tan listo), sino que simplemente he seguido los pasos que aparecen en el howto que tiene gitlab para crear las "gitlab pages" [[⁶]](#ref_6)

Te voy a contar que es lo que tendrías que hacer para poder crear el blog con las gitlab pages.

Lo primero, create una cuenta de gitlab [[⁸]](#ref_8) si aun no la tienes.

Después entra en el grupo "GitLab Pages examples" [[⁹]](#ref_9) para hacer un fork del proyecto hugo [[¹⁰]](#ref_10).

Una vez clonado el repo, elimina la refencia del fork y cambia el nombre por _usuario.gitlab.io_ (en tu caso puse _opeshm.gitlab.io_) y listo.

Solo una cosa que comentar que no te dicen en la documentación (o al menos yo no la vi). Una vez hecho el fork, eliminada la referencia del fork, cambiado el nombre, habiendome cerciorado que el pipeline se habia ejecutado correctamente, tardó un par de horas en que "https://opeshm.gitlab.io" respondiese. Así que si intentas montar tu propia pagina y no responde a la primera el site, ten paciencia.

Y listo, ya tienes montada la página.


---

### Referencias

- <a name="ref_1">[¹]</a> https://en.wikipedia.org/wiki/Static_web_page
- <a name="ref_2">[²]</a> https://www.creativebloq.com/features/10-best-static-site-generators
- <a name="ref_3">[³]</a> https://www.staticgen.com
- <a name="ref_4">[⁴]</a> https://gohugo.io
- <a name="ref_5">[⁵]</a> https://pages.github.com
- <a name="ref_6">[⁶]</a> https://docs.gitlab.com/ee/user/project/pages/
- <a name="ref_7">[⁷]</a> https://gitlab.com/gitlab-org
- <a name="ref_8">[⁸]</a> https://gitlab.com/users/sign_in
- <a name="ref_9">[⁹]</a> https://gitlab.com/pages
- <a name="ref_10">[¹⁰]</a> https://gitlab.com/pages/hugo
