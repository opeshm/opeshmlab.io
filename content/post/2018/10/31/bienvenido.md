---
title: "Bienvenido"
subtitle: "El primer post del blog"
date: 2018-10-30
draft: false
tags: ["hello-world"]
---

Hola visitante! Si estás leyendo esto es que, claramente, te has perdido. Aun así, si todavia tienes ganas de quedarte y seguir leyendo, se bienvenido.

<!--more-->

Bueno, me presento, me llamo <b>CENSORED</b> y me dedico a este precioso mundo que es la informática. Más concretamente al mundo del desarrollo web. Si me preguntas que puesto de trabajo tengo, la verdad, no sabría que decirte. Creo que ahora mismo soy lo que un IT recruiter llamaria DevOps (no voy a entrar a discutir que no es una profesion, ni un puesto de trabajo, ni un perfil, sino
una filosofía).

Actualmente formo parte del equipo de sistemas de una empresa, pero en mi carrera profesional he hecho tanto programación (ASP, ASP.NET, Java, php, python, ...) como administración de servidores, gestion de infrastructura... Un "pa'tó".

¿Y por qué empiezo un blog? Pues mira, siempre he querido empezar un proyecto como este, tener un sitio donde contar los problemas que me encuentro, tener un repositorio de snippets (que da mucho coraje necesitar algo que hiciste hace mil y no encontrarlo)... en definitiva, un cajón de sastre para no perder el tiempo invertido en solventar los problemillas que me encuentro, y ya de paso, si a alguien más le puede servir, pues mejor.

Así que, alla vamos. Y para empezar, en la siguiente entrada explicaré como he montado este mismo blog. No es gran cosa, pero por algo se empieza. Espero poder llenar pronto este blog con temas de un poco más de enjundia.

Lo dicho, se bienvenido y disfruta de tu estancia en este rinconcito de internet que voy al hacer propio.

Ta'lue ;)
